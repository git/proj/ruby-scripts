require 'rubygems/command'
require 'rubygems/install_update_options'

class Gem::Commands::SrcInstallCommand < Gem::Command

  include Gem::InstallUpdateOptions

  def initialize
    super 'src_install', 'Execute Gentoo src_install phase'

    add_install_update_options
  end

  def execute
    get_all_gem_names.each do |gem_name|
      Gemtoo::src_install gem_name, options
    end

  end


end

require 'rubygems/command'
require 'rubygems/install_update_options'

class Gem::Commands::SrcCompileCommand < Gem::Command

  include Gem::InstallUpdateOptions

  def initialize
    super 'src_compile', 'Execute Gentoo src_compile phase'

    add_install_update_options
  end

  def execute
    get_all_gem_names.each do |gem_name|
      Gemtoo::src_compile gem_name, options
    end

  end


end

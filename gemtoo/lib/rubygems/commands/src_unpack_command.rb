require 'rubygems/command'
require 'rubygems/install_update_options'

class Gem::Commands::SrcUnpackCommand < Gem::Command

  include Gem::InstallUpdateOptions

  def initialize
    super 'src_unpack', 'Execute Gentoo src_unpack phase'

    add_install_update_options
  end

  def execute
    get_all_gem_names.each do |gem_name|
      Gemtoo::src_unpack gem_name, options
    end

  end


end

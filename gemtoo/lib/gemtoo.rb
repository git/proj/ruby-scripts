require 'rubygems/installer'
require 'rubygems/config_file'

class Gemtoo
  VERSION = '1.0.0'


  def self.src_unpack(name, options)
    inst = Gem::Installer.new(name, options)
    inst.extract_files
  end


  def self.src_compile(name, options)
    inst = Gem::Installer.new(name, options)
    inst.build_extensions
  end

  def self.src_install(name, options)
    inst = Gem::Installer.new(name, options)
    inst.generate_bin
    inst.write_spec
  end


end

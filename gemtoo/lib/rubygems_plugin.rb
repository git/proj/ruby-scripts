require 'rubygems/command_manager'
require 'gemtoo'

Gem::CommandManager.instance.register_command :src_unpack
Gem::CommandManager.instance.register_command :src_compile
Gem::CommandManager.instance.register_command :src_install

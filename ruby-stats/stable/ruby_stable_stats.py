#!/usr/bin/env python
# date                ruby.eclass     ruby-ng.eclass  ruby18  ruby19  ruby20 jruby ree18 rbx ruby21 ruby22
# 2010-01-20    181             143             141     68      51
# 2010-01-21    180             144             142     68      52
#
date_fmt = "%Y-%m-%d"
bins = ('ruby', 'ruby-ng', 'ruby_targets_ruby18', 'ruby_targets_ruby19', 'ruby_targets_ruby20', 'ruby_targets_jruby', 'ruby_targets_ree18', 'ruby_targets_rbx', 'ruby_targets_ruby21', 'ruby_targets_ruby22')

import sys
import time
import os
import portage

def main():
	stats = {}
	for k in bins:
		stats[k] = set()

	portdb = portage.portdb
	portdb.porttrees = [portdb.porttree_root] # exclude overlays
	for cp in portdb.cp_all():
		slot_dict = {}
		for cpv in portdb.cp_list(cp):
			slot, iuse, inherited, keywords = portdb.aux_get(cpv, ['SLOT', 'IUSE', 'INHERITED', 'KEYWORDS'])
			if  "amd64" in keywords and not "~amd64" in keywords:
				slot_dict.setdefault(slot, {})[cpv] = (iuse, inherited)
		for slot, cpv_dict in slot_dict.items():
			cpv = portage.best(list(cpv_dict))
			if cpv:
				iuse, inherited = cpv_dict[cpv]
				iuse = set(iuse.split())
				inherited = set(inherited.split())
				if 'ruby' in inherited:
					stats['ruby'].add(cpv)
				if 'ruby-ng' in inherited:
					stats['ruby-ng'].add(cpv)
					if 'ruby_targets_ruby18' in iuse:
						stats['ruby_targets_ruby18'].add(cpv)
					if 'ruby_targets_ruby19' in iuse:
						stats['ruby_targets_ruby19'].add(cpv)
					if 'ruby_targets_ruby20' in iuse:
						stats['ruby_targets_ruby20'].add(cpv)
					if 'ruby_targets_ruby21' in iuse:
						stats['ruby_targets_ruby21'].add(cpv)
					if 'ruby_targets_ruby22' in iuse:
						stats['ruby_targets_ruby22'].add(cpv)
					if 'ruby_targets_jruby' in iuse:
						stats['ruby_targets_jruby'].add(cpv)
					if 'ruby_targets_ree18' in iuse:
						stats['ruby_targets_ree18'].add(cpv)
					if 'ruby_targets_rbx' in iuse:
						stats['ruby_targets_rbx'].add(cpv)

	date_stamp = time.strftime(date_fmt, time.gmtime())
	counts = [str(len(stats[x])) for x in bins]
	sys.stdout.write("%s\t%s\n" % (date_stamp, "\t".join(counts)))

if __name__ == '__main__':
	main()
